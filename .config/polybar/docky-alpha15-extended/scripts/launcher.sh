#!/usr/bin/env bash

rofi -no-config -no-lazy-grab -show drun -modi drun -theme ~/.config/polybar/docky-alpha15-extended/scripts/rofi/launcher.rasi
