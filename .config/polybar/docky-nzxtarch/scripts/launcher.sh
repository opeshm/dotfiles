#!/usr/bin/env bash

rofi -no-config -no-lazy-grab -show drun -modi drun -theme ~/.config/polybar/docky-nzxtarch/scripts/rofi/launcher.rasi
