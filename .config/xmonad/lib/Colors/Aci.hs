module Colors.Aci where

import XMonad

colorScheme = "aci"

colorBack = "#282c34"
colorFore = "#bbc2cf"
color01 = "#363636"
color02 = "#ff0883"
color03 = "#83ff08"
color04 = "#ff8308"
color05 = "#0883ff"
color06 = "#8308ff"
color07 = "#08ff83"
color08 = "#b6b6b6"
color09 = "#424242"
color10 = "#ff1e8e"
color11 = "#8eff1e"
color12 = "#ff8e1e"
color13 = "#1e8eff"
color14 = "#8e1eff"
color15 = "#1eff8e"
color16 = "#c2c2c2"

colorTrayer :: String
colorTrayer = "--tint 0x282c34"
